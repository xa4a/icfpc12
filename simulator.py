#!/usr/bin/env pypy

import sys
import copy
import logging

from common import load_map, State, Input, print_map, find_cells


logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger(__name__)


WIN, LOSE, ABORT = range(3)

class Finished(Exception):
    pass

class Simulator(object):
    def __init__(self, input_commands, input, underwater=0, verbose=False):
        self._h = len(input.map)
        self._w = len(input.map[0])

        init_state = State(input, self.find_robot(input.map), underwater=underwater,
                           score=0)
        self._commands = input_commands
        self._verbose = verbose

        self.history = [init_state]

    def find_robot(self, map=None):
        if map is None:
            map = self.history[-1].map
        for i in range(len(map)):
            for j in range(len(map[i])):
                if map[i][j] == 'R':
                    return (i, j)


    def simulate_step(self, state, command):
        if not command in 'AW':
            after_command_state = self.execute_command(state, command)
        else:
            after_command_state = state

        after_map_update_state = self.map_update(after_command_state)

        if after_map_update_state.robot[0] <= state.input.metadata['Water']:
            after_map_update_state = after_map_update_state._replace(
                    underwater=after_map_update_state.underwater+1)
        else:
            after_map_update_state = after_map_update_state._replace(
                    underwater=0)
        #print 'robot', after_map_update_state.robot[0]
        #print 'water' ,state.input.metadata['Water']
        #print 'under', after_map_update_state.underwater


        if command == 'A':
            end = ABORT
        else:
            end = self.ending_conditions(after_command_state,
                                         after_map_update_state)
        self.history.append(after_map_update_state)
        if end is not None:
            raise Finished(end, after_map_update_state)
        return after_map_update_state

    def ending_conditions(self, old_state, new_state):
        win = True
        for i in new_state.input.map:
            for j in i:
                if j in 'OL':
                    win = False
                    break
            else:
                continue
            break
        if win:
            return WIN

        # Falling stone.
        i, j = new_state.robot
        if i < self._h - 2:
            if (old_state.input.map[i+1][j] == ' ' and
                new_state.input.map[i+1][j] in '@*'):
                return LOSE

        # Drown
        if new_state.underwater > new_state.input.metadata['Waterproof']:
            #print 'drown'
            return LOSE

    def map_update(self, state):
        m = state.input.map
        h, w = len(m), len(m[0])
        n = copy.deepcopy(m)
        new_metadata = copy.deepcopy(state.input.metadata)
        grow_beard = new_metadata['Growth_current'] == 0
        if grow_beard:
            new_metadata['Growth_current'] = new_metadata['Growth'] - 1
        else:
            new_metadata['Growth_current'] -= 1

        for i in range(self._h):
            for j in range(self._w):
                # falling rock
                if i > 0 and m[i][j] in '@*' and m[i-1][j] == ' ':
                    n[i][j] = ' '
                    n[i-1][j] = m[i][j]

                    if i > 1 and m[i][j] == '@' and m[i-2][j] != ' ':
                        n[i-1][j] = '\\'

                # rock slides right
                elif i > 0 and j < self._w-1 and (
                        m[i][j] in '@*' and
                        m[i-1][j] in '@*' and
                        m[i][j+1] == ' ' and
                        m[i-1][j+1] == ' '):
                    n[i-1][j+1] = m[i][j]
                    if i > 1 and m[i][j] == '@' and m[i-2][j+1] != ' ':
                        n[i-1][j+1] = '\\'
                    n[i][j] = ' '


                # rock slides left
                elif i > 0 and j > 0 and (
                        m[i][j] in '@*' and
                        m[i-1][j] in '@*' and
                        m[i][j-1] == ' ' and
                        m[i-1][j-1] == ' '):
                    n[i-1][j-1] = m[i][j]
                    if i > 1 and m[i][j] == '@' and m[i-2][j-1] != ' ':
                        n[i-1][j-1] = '\\'
                    n[i][j] = ' '

                # rock slides right from lambda
                elif i > 0 and j < self._w-1 and (
                        m[i][j] in '@*' and
                        m[i-1][j] == '\\' and
                        m[i][j+1] == ' ' and
                        m[i-1][j+1] == ' '):
                    n[i-1][j+1] = m[i][j]
                    if i > 1 and m[i][j] == '@' and m[i-2][j+1] != ' ':
                        n[i-1][j+1] = '\\'
                    n[i][j] = ' '

                #TODO: count lambdas before opening lift
                if m[i][j] == 'L':
                    for row in m:
                        for cell in row:
                            if cell in '@\\':
                                break
                        else:
                            continue
                        break
                    else:
                        n[i][j] = 'O'
                # maybe grow beard
                if m[i][j] == ' ' and grow_beard:
                    for dx, dy in [(-1,-1),(-1,0),(-1,1),(0,1),(1,1),(1,0),(1,-1),(0,-1)]:
                        nx, ny = i+dx, j+dy
                        if 0 <= nx < h and 0 < ny < w and m[nx][ny] == 'W':
                            n[i][j] = 'W'

        if new_metadata['Flooding']:
            new_metadata['Water'] += 1./new_metadata['Flooding']
        return state._replace(input=Input(map=n, metadata=new_metadata))


    def execute_command(self, state, command):
        robot = state.robot
        r_i, r_j = robot
        new_map = copy.deepcopy(state.input.map)
        new_metadata = copy.deepcopy(state.input.metadata)

        score = 0
        if command == 'L':
            if r_j == 0:
                # Noop, at left edge
                return state
            target_coord = (r_i, r_j-1)
            target = new_map[target_coord[0]][target_coord[1]]
            if target in '123456789#':
                return state
            if target in '@*' and r_j > 1 and new_map[r_i][r_j-2] == ' ':
                # push rock
                new_map[r_i][r_j-2] = target
                new_map[r_i][r_j-1] = 'R'
                new_map[r_i][r_j]   = ' '
                robot = target_coord


        elif command == 'R':
            if r_j == self._w-1:
                return state
            target_coord = (r_i, r_j+1)
            target = new_map[target_coord[0]][target_coord[1]]
            if target in '123456789#':
                return state
            if target in '@*' and r_j < self._w - 2 and new_map[r_i][r_j+2] == ' ':
                # push rock
                new_map[r_i][r_j+2] = target
                new_map[r_i][r_j+1] = 'R'
                new_map[r_i][r_j]   = ' '
                robot = target_coord
        elif command == 'U':
            if r_i == self._h:
                return state
            target_coord = (r_i+1, r_j)
            target = new_map[target_coord[0]][target_coord[1]]
        elif command == 'D':
            if r_i == 0:
                return state
            target_coord = (r_i-1, r_j)
            target = new_map[target_coord[0]][target_coord[1]]

        if command in 'UDLR':
            if target in '123456789':
                return state
            if target in 'ABCDEFGHI':
                # teleport
                new_map[r_i][r_j] = ' '
                target = state.input.metadata['Trampolines'][target]
                robot = target_coord = find_cells(new_map, target)[0]
                tramps_to_remove = [k for (k, v) in state.input.metadata['Trampolines'].items()
                                    if v == target]
                for t in tramps_to_remove:
                    for row, col in find_cells(new_map, t):
                        new_map[row][col] = ' '

        if command == 'S':
            target = 'R'
            target_coord = robot
            if new_metadata['Razors']:
                new_metadata['Razors'] -= 1
                for dx, dy in [(-1,-1),(-1,0),(-1,1),(0,1),(1,1),(1,0),(1,-1),(0,-1)]:
                    r, c = r_i + dx, r_j + dy
                    if new_map[r][c] == 'W':
                        new_map[r][c] = ' '

        #print command
        if target in ' .\\O123456789!':
            if target == '\\':
                score = 1
            elif target == '!':
                new_metadata['Razors'] += 1
            new_map[r_i][r_j] = ' '
            new_map[target_coord[0]][target_coord[1]] = 'R'
            robot = target_coord

        return state._replace(input=state.input._replace(map=new_map,
                                                         metadata=new_metadata),
                              robot=robot, score=state.score + score)

    def simulate(self, interactive=False, skip=0, do_abort=True):
        state = self.history[-1]
        success = None
        moves = 0
        score = 0
        try:
            commands_done = []
            for command in self._commands:
                #print command
                if command != 'A':
                    moves += 1
                state = self.simulate_step(state, command)
                if interactive and moves > skip:
                    commands_done.append(command)
                    print ''.join(commands_done)
                    print_map(state.input)
                    c = raw_input("return to step, s to skip\n")
                    if c == 's':
                        interactive = False
            else:
                if self._verbose:
                    print 'bot did not finish.'
                # Implicitly abort if the bot program is finished and no result.
                if do_abort:
                    state = self.simulate_step(state, 'A')
            lambdas = state.score
        except Finished as e:
            res, state = e.args
            lambdas = state.score
            if res == WIN:
                score += lambdas * 50
                if self._verbose:
                    print 'you win'
                success = True
            elif res == LOSE:
                if self._verbose:
                    print 'you lose'
                success = False
            elif res == ABORT:
                score += lambdas * 25
                if self._verbose:
                    print 'abort'

        score -= moves
        score += lambdas * 25
        if self._verbose:
            print 'score: ' + str(score)
        return success, state._replace(score=score)


def simulate(input_commands, map, interactive=False, skip=0, verbose=False):
    s = Simulator(input_commands, map, verbose=verbose)
    return s.simulate(interactive, skip)

def simulate_one_step(map, step, do_abort=False, underwater=0, verbose=False):
    return Simulator(step, map, underwater, verbose=verbose).simulate(do_abort=do_abort)

def hash_map(map):
    return '\n'.join(''.join(row) for row in map)

def optimize_solution(input, solution):
    seen_states = {}
    i = 0
    res = []
    last_increase = 0
    best_score = 0
    for n, command in enumerate(solution):
        success, new_state = simulate_one_step(input, solution[:n+1],
                                               verbose=False)
        h = hash_map(new_state.input.map)
        if h in seen_states:
            i = seen_states[h]
            del res[i:]

        seen_states[h] = i

        res.append(command)
        i += 1

        if new_state.score > best_score:
            last_increase = i
            best_score = new_state.score
    if last_increase < i:
        res = res[:last_increase]
    return ''.join(res)



def main():
    if len(sys.argv) < 3:
        print 'USAGE: simulator.py mapfile <COMMANDS>'
        sys.exit(1)
    map_file, commands = sys.argv[1:3]
    interactive = '-i' in sys.argv
    skip = 0
    if '-s' in sys.argv:
        skip = int(sys.argv[sys.argv.index('-s') + 1])
    map = load_map(map_file)
    print 'start map:'
    print map
    res, state = simulate(commands, map, interactive, skip, verbose=True)
    print 'end map:'
    print state.input
    print optimize_solution(map, commands)


if __name__ == '__main__':
    main()
