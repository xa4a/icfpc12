#!/usr/bin/env pypy

import collections
import logging
import threading
import sys
import copy
import math
import signal
from itertools import combinations_with_replacement

logging.basicConfig(level=logging.INFO)
import simulator
from common import (load_map, find_cells, teleports_to, find_pattern, Target)


directions = [(0, 1), (0, -1), (-1, 0), (1, 0)]
commands = {
        (1, 0): 'U',
        (0, 1): 'R',
        (-1, 0): 'D',
        (0, -1): 'L',
        (0, 0): 'W',
}

BLOCKED = 100000
# If it's pushable, it gets lower score in distance()
ROCK = 100000
BEARD = 5000
EARTH = 3
RAZOR = 1

FASTER = True

def is_done(input):
    win = True
    for i in input.map:
        for j in i:
            if j in 'OL':
                win = False
                break
        else:
            continue
        break
    if win:
        return True

    # If blocked return True

def score_map(map):
    h, w = len(map), len(map[0])
    robot = find_cells(map, 'R')[0]

    def find_targets(pattern, pos, priority, mapping={}):
        return [Target(priority, v[0], v[1]) for
                v in find_pattern(pattern, pos, map, mapping)]

    targets = find_targets(['\\'], [0, 0], -1)

    #--- priority = 0
    # break horocks
    # dig under
    targets += find_targets(['O'], [0,0], 0)

    pattern = ["@",
               "."][::-1]
    targets += find_targets(pattern, [0,0], 0)

    # dig to the down-right
    pattern = ["@ ",
               "r."][::-1]
    mapping = {'r': 'R*@\\'}
    targets += find_targets(pattern, [0,1], 0, mapping)

    # dig down-left
    pattern = [" @",
               ".l"][::-1]
    mapping = {'l': 'R*@'}
    targets += find_targets(pattern, [0,0], 0, mapping)

    # move left from under the horock
    pattern = ["-@",
               "eR"][::-1]
    mapping = {'e': '. '}
    targets += find_targets(pattern, [0,0], 0, mapping)

    # move right from under the horock
    pattern = ["@-",
               "Re"][::-1]
    mapping = {'e': '. '}
    targets += find_targets(pattern, [0,1], 0, mapping)

    #--- priority = 1
    # push horocks
    # right
    pattern = ["R@ ",
               "-w-"][::-1]
    mapping = {'w': '#W'}
    targets += find_targets(pattern, [1,1], 0, mapping)
    # left
    pattern = [" @R",
               "-w-"][::-1]
    mapping = {'w': '#W'}
    targets += find_targets(pattern, [1,1], 0, mapping)

    # go to pushing horocks
    # right
    pattern = ["e@ ",
               "-w-"][::-1]
    mapping = {'w': '#W',
               'e': '. '}
    targets += find_targets(pattern, [1,0], 1, mapping)
    # left
    pattern = [" @e",
               "-w-"][::-1]
    mapping = {'w': '#W',
               'e': ' .'}
    targets += find_targets(pattern, [1,2], 1, mapping)


    # If no success, get from under the rock
    # right
    pattern = ["*-",
               "Re"][::-1]
    mapping = {'e': '. '}
    targets += find_targets(pattern, [0,1], 1, mapping)
    #left
    pattern = ["-*",
               "eR"][::-1]
    mapping = {'e': '. '}
    targets += find_targets(pattern, [0,0], 1, mapping)

    # Dig random rock
    pattern = ["*",
               "."][::-1]
    targets += find_targets(pattern, [0,0], 2)

    cost_map = copy.deepcopy(map)

    costs = {
            'W': BEARD,
            'L': BLOCKED,
            '#': BLOCKED,
            '*': ROCK,
            '@': ROCK,
            '!': RAZOR,
            '.': EARTH,
            ' ': EARTH,
    }
    for i, row in enumerate(cost_map):
        for j, cell in enumerate(row):
            cost_map[i][j] = costs.get(cost_map[i][j], EARTH)

    return robot, targets, cost_map

def heuristic(p1, p2):
    drow = p2[0] - p1[0]
    dcol = p2[1] - p1[1]
    return math.hypot(drow, dcol)


def distance(scored_map, current, neighbor, real_map):
    h, w = len(real_map), len(real_map[0])

    drow = neighbor[0] - current[0]
    dcol = neighbor[1] - current[1]
    if real_map[neighbor[0]][neighbor[1]] in '@*':
        # If the rock can be pushed - assume it's earth, but more expensive.
        h, w = len(scored_map), len(scored_map[0])
        next_one = current[0] + drow * 2, current[1] + dcol * 2
        if (drow == 0 and dcol in [1, -1] and  # Only push horizontally.
            0 <= next_one[0] < h and 0 <= next_one[1] < w):
            if real_map[next_one[0]][next_one[1]] in ' R':
                return 10

    # Don't go down, if we're below a rock
    if (0 < current[0] < h - 1
            and (drow, dcol) == (-1, 0) # going down
            and real_map[current[0]+1][current[1]] in '@*' # above is rock
            and real_map[neighbor[0]][neighbor[1]] in 'R .\\'): # to empty
        return BLOCKED

    # Don't go down if we're below a rock falling from the left.
    if (0 < current[0] < h - 1  # not top cell
            and 0 < current[1] <= w - 1  # not left cell
            and (drow, dcol) == (-1, 0) # going down
            and real_map[current[0]+1][current[1]] in 'R ' # above is empty
            and real_map[neighbor[0]][neighbor[1]] in 'R .\\' # below is free
            and real_map[current[0]][current[1]-1] in '\\*@' # to the left is rock or \\
            and real_map[current[0]+1][current[1]-1] in '*@' # up-left is rock
            ):
        return BLOCKED

    # Don't go down if we're below a rock falling from the right.
    if (0 < current[0] < h - 1  # not top cell
            and 0 <= current[1] < w - 1  # not right cell
            and (drow, dcol) == (-1,0) # going down
            and real_map[current[0]+1][current[1]] in 'R ' # above is empty
            and real_map[neighbor[0]][neighbor[1]] in 'R .\\' # below is free
            and real_map[current[0]][current[1]+1] in '@*' # to the right is stone
            and real_map[current[0]+1][current[1]+1] in '@*' # up-right is rock
            ):
        return BLOCKED


    return scored_map[neighbor[0]][neighbor[1]]

def path_cost(path, scored_map, map):
    sum = 0
    for i, j in zip(path[:-1], path[1:]):
        sum += distance(scored_map, i, j, map)
    return sum

def decide(input, skip_lambdas=None, target=None):
    skip_lambdas = skip_lambdas or set()

    map = input.map
    start, targets, scored_map = score_map(map)

    if not targets or set(targets) <= skip_lambdas:
        return [start], skip_lambdas, None
    h, w = len(scored_map), len(scored_map[0])
    open_set = set([start])
    closed_set = set()
    came_from = {}

    g_score = collections.defaultdict(lambda: 10000)
    f_score = {}

    g_score[start] = 0

    # If hunting real lambdas, prefer farthest from lift
    lift = (find_cells(input.map, 'L') or find_cells(input.map, 'O'))[0]
    if target is None:
        closest_target = (targets[i]
                          for i in get_target_order(input, start, targets, lift)
                          if targets[i] not in skip_lambdas).next()
    else:
        closest_target = target

    f_score[start] = heuristic(start, closest_target[1:])

    while open_set:
        #TODO: use heap for openset instead.
        min_score, current = min((f_score[x], x) for x in open_set)
        if current == closest_target[1:]:
            path = reconstruct_path(came_from, current)
            if path_cost(path, scored_map, map) >= BLOCKED:
                #print 'path',  reconstruct_path(came_from, current)
                # Didn't find any way to the closest lambda.

                # Try removing the closest one
                skip_lambdas.add(closest_target)

                path, skip_lambdas, target = decide(input, skip_lambdas, target=None)
                return path, skip_lambdas, target

            else:
                # if path block starts with beard
                shavable = None
                for i, (x, y) in enumerate(path):
                    if scored_map[x][y] == BLOCKED:
                        shavable = False
                        break
                    if scored_map[x][y] == BEARD:
                        shavable = True
                        break
                if shavable:
                    if i > 1:
                        # Keep calm, carry on to beard!
                        return path, skip_lambdas, closest_target
                    else:
                        # Here comes the beard!
                        if input.metadata['Razors']:
                            # Approach it
                            return path, skip_lambdas, closest_target

            # Target actually reached
            return path, skip_lambdas, closest_target
        open_set.remove(current)

        closed_set.add(current)
        for dx, dy in directions:
            nx, ny = current[0] + dx, current[1] + dy
            if not (0 <= nx < h and 0 <= ny < w):
                continue
            cell = input.map[nx][ny]
            if cell in 'ABCDEFGHI':
                target = input.metadata['Trampolines'][cell]
                nx, ny = teleports_to((nx, ny), input)
            neighbor = (nx, ny)
            if neighbor in closed_set:
                continue
            tentative_g_score = (g_score[current] +
                                 #scored_map[neighbor[0]][neighbor[1]])
                                 distance(scored_map, current, neighbor, map))

            if neighbor not in open_set or tentative_g_score < g_score[neighbor]:
                open_set.add(neighbor)
                came_from[neighbor] = current
                g_score[neighbor] = tentative_g_score
                f_score[neighbor] = g_score[neighbor] + heuristic(
                        neighbor, closest_target[1:])
    # No way to the closest lambda
    return None

def get_command(src, dst, input=None):
    dx, dy = (dst[0]-src[0]), (dst[1]-src[1])
    nx, ny = src[0] + dx, src[1] + dy
    if input.map[nx][ny] == 'W':
        return 'S'
    if (dx, dy) in commands:
        return commands[(dx,dy)]
    # Trampolined
    for (dx, dy), command in commands.items():
        nx, ny = src[0] + dx, src[1] + dy
        if input.map[nx][ny] in 'ABCDEFGHI':
            if teleports_to((nx,ny), input) == (dst[0], dst[1]):
                return command

def follow_nearest_score(input, start, targets_, lift):
    targets = set(targets_)
    score = 0
    order = []
    if FASTER:
        paths_f = stupid_paths
    else:
        paths_f = dijkstra_paths
    paths = dict(zip(targets, paths_f(input, start, [l[1:] for l in targets])))
    while targets:
        distances = []
        for l in targets:
            path = paths[l]
            prio = l[0]
            if path is None:
                dist = 1000
            else:
                dist = len(path)-1
            #dist = heuristic(start, l[1:])
            distances.append( ((prio, dist), l) )

        d, nearest = min(distances)
        score += d[1]
        targets.remove(nearest)
        start = nearest
        order.append(targets_.index(start))
    score += heuristic(start, lift)
    return score, order

def farthest_from_lift_score(input, start, targets_, lift):
    targets = set(targets_)
    score = 0
    order = []
    if FASTER:
        paths_f = stupid_paths
    else:
        paths_f = dijkstra_paths
    paths = dict(zip(targets, paths_f(input, start, [l[1:] for l in targets])))
    while targets:
        distances = []
        for l in targets:
            path = paths[l]
            prio = l[0]
            if path is None:
                dist = 1000
            else:
                dist = len(path)-1
            #dist = heuristic(start, l[1:])
            distances.append( ((-prio, dist), l) )

        d, farthest = max(distances)
        score += d[1]
        targets.remove(farthest)
        start = farthest
        order.append(targets_.index(start))
    score += heuristic(start, lift)
    return score, order


def get_target_order(input, start, targets, lift):
    score1, order1 = follow_nearest_score(input, start, targets, lift)
    score2, order2 = farthest_from_lift_score(input, start, targets, lift)

    return order1
    #if score1 < score2:
    #    return order1
    #else:
    #    return order2

def reconstruct_path(came_from, current_node):
    if current_node in came_from:
        p = reconstruct_path(came_from, came_from[current_node])
        return p + [current_node]
    else:
        return [current_node]

def dijkstra(input, source):
    map = input.map
    h, w = len(map), len(map[0])
    scored_map = score_map(map)[2]

    dist = collections.defaultdict(lambda: 10000000)
    previous = collections.defaultdict(lambda: None)

    dist[source] = 0
    Q = set([(i, j) for i in range(h) for j in range(w)])
    while Q:
        u = min((dist[q], q) for q in Q)[1]
        if dist[u] == 10000000:
            break

        Q.remove(u)

        for dx, dy in directions:
            nx, ny = u[0] + dx, u[1] + dy
            if not (0 <= nx < h and 0 <= ny < w):
                continue
            cell = input.map[nx][ny]
            if cell in 'ABCDEFGHI':
                nx, ny = teleports_to((nx, ny), input)
            v = (nx, ny)
            alt = dist[u] + distance(scored_map, u, v, map)
            if alt < dist[v]:
                dist[v] = alt
                previous[v] = u
    return dist, previous

def dijkstra_paths(input, source, goals):
    dist, previous = dijkstra(input, source)

    paths = []
    for goal in goals:
        S = []
        u = goal
        while u:
            S.append(u)
            u = previous[u]
            u = previous[u]
        paths.append(S[::-1])
    return paths

def stupid_paths(input, src, goals):
    paths = []
    for goal in goals:
        dst = int(heuristic(src, goal))
        paths.append([0]*dst)
    return paths

def solve(input, underwater=0, strip=True):
    res = []
    success = None
    i = 0
    underwater = underwater
    skip_lambdas = set()
    target = None
    original_input = input
    while (not is_done(input)) and success != False:
        i += 1
        # while we don't take the dynamical aspects of map into account,
        # have to rerun A* on every iteration
        #import ipdb;ipdb.set_trace()
        path, skip_lambdas, target = decide(input, skip_lambdas, target)
        if len(path) < 2:
            command = 'W'
        else:
            command = get_command(path[0], path[1], input)
        success, new_state = simulator.simulate_one_step(input, command,
                                                         underwater=underwater,
                                                         do_abort=False)
        if success == False:
            res.append('A')
            break

        new_input = new_state.input
        underwater = new_state.underwater

        res.append(command)
        #print "".join(res)

        # if got a lambda on this step
        if len(path) == 2:
            skip_lambdas = set()
            target = None

        changed_cells = []
        for i, row in enumerate(new_input.map):
            for j, _ in enumerate(row):
                if new_input.map[i][j] != input.map[i][j]:
                    changed_cells.append((i,j))

        # if something has fallen, reset target
        # TODO: this is also triggered on shaving
        if len(changed_cells) > 2:
            target = None

        if not changed_cells:
            break
        input = new_input
    ret = ''.join(res)
    if strip:
        ret = simulator.optimize_solution(original_input, ret)
    return ret

def solve_iter(input, underwater=0, known_results=set(), known_histories=set()):
    init_solution = solve(input)
    yield init_solution

    # If the first solution is really shitty, try to iterate its unstripped
    # version
    if not init_solution:
        init_solution = solve(input, strip=False)

    init_solution = init_solution.rstrip('AW')
    if not init_solution:
        return

    head = init_solution
    tail_len = 0
    best_score = -1000

    while True:
        if tail_len:
            head = init_solution[:-tail_len]
        else:
            head = init_solution
        #print head
        tail_len += 1
        for tail in combinations_with_replacement('URDL', tail_len):
            alternative_history = head + ''.join(tail)
            if alternative_history in known_histories:
                continue
            known_histories.add(alternative_history)
            _, alternative_state = simulator.simulate_one_step(input,
                                                               alternative_history,
                                                               underwater=underwater,
                                                               do_abort=False)
            more_tail = solve(alternative_state.input, underwater=alternative_state.underwater, strip=False).strip('AW')
            alternative_solution = alternative_history + more_tail

            if alternative_solution in known_results:
                continue
            else:
                known_results.add(alternative_solution)
            #print alternative_solution
            _, new_state = simulator.simulate_one_step(input, alternative_solution,
                                                       underwater=underwater)
            # Randomly found a better solution = unstuck.
            if new_state.score > best_score:
                best_score = new_state.score
                yield alternative_solution
                # go deeper, allow some more fuzzyness
                for more_tail in solve_iter(new_state.input,
                                            underwater=new_state.underwater):
                    res = alternative_solution + more_tail
                    yield res

            yield res
        if not head:
            break

def signal_handler(generator):
    def inner(signal, frame):
        with solution_lock:
            print best_solution[0][0]
        generator.throw(StopIteration)
    return inner

def dump_current(signal, frame):
    sys.stderr.write('%s\n' % best_solution[0][0])

best_solution = [("", -1000)]
solution_lock = threading.RLock()

def main(map=None):

    if map is None:
        map_file = sys.argv[1]
        map = load_map(map_file)

    solutions_generator = solve_iter(map)
    signal.signal(signal.SIGINT, signal_handler(solutions_generator))
    signal.signal(signal.SIGUSR1, dump_current)

    try:
        for solution in solutions_generator:
            res, state = simulator.simulate(solution, map)
            score = state.score
            #print solution, score
            with solution_lock:
                if score > best_solution[0][1]:
                    best_solution[0] = (solution, score)
        print best_solution[0][0]
    except ValueError as e:
        if e.args[0] != 'generator already executing':
            raise
    except StopIteration:
        pass

if __name__ == '__main__':
    main()
