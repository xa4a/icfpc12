#!/usr/bin/env pypy
import logging
import subprocess
import sys

logging.basicConfig(level=logging.INFO)
import solve, common, simulator

def main():
    if len(sys.argv) < 2:
        print 'Usage ./solve_and_run.py <map_file> [-i]'
        sys.exit(1)
    map_file = sys.argv[1]
    map = common.load_map(map_file)
    if False:
        solution = solve.solve(map)
    else:
        cmd = 'echo $(timeout -2 30 ./solve.py %s  2> /dev/null)' % map_file
        p = subprocess.Popen(['/usr/bin/bash', '-c', cmd], stdout=subprocess.PIPE)
        (out, err) = p.communicate()
        solution = out.strip()

    interactive = '-i' in sys.argv
    skip = 0
    if '-s' in sys.argv:
        skip = int(sys.argv[sys.argv.index('-s') + 1])
    res, state = simulator.simulate(solution, map, interactive, skip=skip)
    logging.basicConfig(level=logging.INFO)
    print solution
    if res:
        print 'WIN!',
    elif res == False:
        print 'Smashed :(',
    else:
        print 'Stuck.',
    print 'Score:', state.score

if __name__ == '__main__':
    main()
