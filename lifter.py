#!/usr/bin/env pypy
import sys
import logging

logging.basicConfig(level=logging.WARN)
import solve
import common

def main():
    map_data = sys.stdin.read()
    map = common.load_map(None, map_data)

    solve.main(map)


if __name__ == '__main__':
    main()



