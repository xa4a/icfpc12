#!/bin/sh

TEAM_ID=96699684
TEAM_NAME="A"
TGZ_NAME="icfp-$TEAM_ID.tgz"

mkdir -p $TEAM_NAME/src
cp *.py $TEAM_NAME/src
cp README install lifter PACKAGES $TEAM_NAME/
cp -a debs maps $TEAM_NAME/

tar czvf $TGZ_NAME $TEAM_NAME
