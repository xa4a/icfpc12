import collections
import itertools


State = collections.namedtuple('State', 'input robot underwater score')
Input_ = collections.namedtuple('Input', 'map metadata')
Target = collections.namedtuple('Target', 'priotiy x y')

class Input(Input_):
    def __repr__(self):
        print_map(self)
        return ''


def load_map(filename, content=None):
    if content is None:
        content = open(filename).read().splitlines()
    else:
        content = content.splitlines()
    map_content = list(itertools.takewhile(lambda x: x, content))
    metadata_content = itertools.dropwhile(lambda x: x, content)
    metadata = {
            'Water': 0,
            'Flooding': 0,
            'Waterproof': 10,
            'Trampolines': {},
            'Growth': 25,
            'Razors': 0,
    }
    for l in metadata_content:
        if l.strip():
            if l.startswith('Trampoline'):
                src, dst = l[11], l[21]
                metadata['Trampolines'][src] = dst
                #print src, dst
            else:
                name, value = l.split()
                #print l
                metadata[name] = int(value)

    metadata['Water'] -= 1
    metadata['Growth_current'] = metadata['Growth'] - 1

    max_len = max(len(l) for l in map_content)
    res = [list(l.ljust(max_len, ' ')) for l in map_content][::-1]
    return Input(res, metadata)


def print_map(input_or_map):
    if isinstance(input_or_map, Input):
        water_level = input_or_map.metadata['Water']
        map = input_or_map.map
    else:
        water_level = -1
        map = input_or_map
    for i, l in reversed(list(enumerate(map))):
        water = '~' if (water_level >= i) else ''
        print ''.join(l) + water
        i += 1

def find_cells(map, cell_type):
    res = []
    for i, row in enumerate(map):
        for j, cell in enumerate(row):
            if cell == cell_type:
                res.append((i, j))
    return res

def find_pattern(pattern, target_coord_in_pattern, map, mapping={}):
    """pattern is a small square map which is matched against windows of real
       map.
       mapping: dict, specifying mapping of pattern symbols to map cells.
    """

    w, h = len(map), len(map[0])
    w2, h2 = len(pattern), len(pattern[0])

    default_mapping = {'-': '#. \\RW!@LO'}
    default_mapping.update(mapping)
    trans = default_mapping

    res = []
    for wrow in range(0, w-w2+1):
        for wcol in range(0, h-h2+1):
            for i in range(w2):
                for j in range(h2):
                    if map[wrow+i][wcol+j] not in trans.get(pattern[i][j], pattern[i][j]):
                        break
                else:
                    # Whole window row matched. get to next window row.
                    continue
                break
            else:
                # All window rows matched
                res.append((wrow+target_coord_in_pattern[0],
                            wcol+target_coord_in_pattern[1]))
            # Move window further
    return res


def teleports_to(src, input):
    target = input.metadata['Trampolines'][input.map[src[0]][src[1]]]
    return find_cells(input.map, target)[0]

